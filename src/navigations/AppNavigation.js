import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Routes from './Routes';
import navigationOptions from './config';

const TabNavigator = createBottomTabNavigator(Routes, navigationOptions);

export default createAppContainer(TabNavigator);