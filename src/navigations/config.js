import React from 'react';
import {
    Image
} from 'react-native';

import TabBar from '../components/TabBar';
import Color from '../themes/Colors';
import Images from '../themes/Images';

const navigationOptions = {
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let source;
            if (routeName === 'Home') {
              source = Images.homeIcon;
            } else if (routeName === 'Search') {
              source = Images.searchIcon;
            } else if (routeName === 'Alarm') {
              source = Images.alarmIcon;
            } else if (routeName === 'Favorite') {
              source = Images.favoriteIcon;
            } else if (routeName === 'Profile') {
              source = Images.profileIcon;
              return <Image source={source} style={{width: 25, height: 25}} />;
            }

            return <Image source={source} style={[{width: 22, height: 22}, {tintColor: tintColor}]} />;
        }
    }),
    tabBarComponent: TabBar,
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: 'white',
        showLabel: false,
        style: {
          borderColor: 'transparent',
          backgroundColor: Color.bottomBarColor
        }
    }
};

export default navigationOptions