import Home from '../containers/Home';
import Search from '../containers/Search';
import Alarm from '../containers/Alarm';
import Favorite from '../containers/Favorite';
import Profile from '../containers/Profile';

const Routes = {
    Home: Home,
    Search: Search,
    Alarm: Alarm,
    Favorite: Favorite,
    Profile: Profile
}

export default Routes;