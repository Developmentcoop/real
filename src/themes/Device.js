import { 
    Dimensions,
    Platform
} from 'react-native';

const dim = Dimensions.get('window');

export const isIphoneX = () => {
    return Platform.OS === 'ios' && (isIphoneXRSize || isIphoneXSize);
}

export const isIphoneXSize = () => {
    return dim.height == 812 || dim.height == 812;
}

export const isIphoneXRSize = () => {
    return dim.height == 896 || dim.height == 896;
}