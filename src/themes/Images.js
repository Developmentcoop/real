const images = {
    homeIcon: require('../assets/icon-images/home.png'),
    searchIcon: require('../assets/icon-images/search.png'),
    alarmIcon: require('../assets/icon-images/alarm.png'),
    favoriteIcon: require('../assets/icon-images/favorite.png'),
    profileIcon: require('../assets/icon-images/profile.png'),
    plusIcon: require('../assets/icon-images/plus.png'),
    messageIcon: require('../assets/icon-images/message.png'),
    commentIcon: require('../assets/icon-images/comment.png'),
    likeIcon: require('../assets/icon-images/like.png'),
    shareIcon: require('../assets/icon-images/share.png'),
    
    mediumProfile: require('../assets/images/medium-profile.png'),
    cardImage: require('../assets/images/card-image.png'),
    posterImage: require('../assets/images/poster-image.png'),
    swipeLeft: require('../assets/images/swipe-left.png'),
    swipeRight: require('../assets/images/swipe-right.png'),
    cardImage1: require('../assets/images/card-image.png'),
    cardImage2: require('../assets/images/card-image2.jpg'),
    cardImage3: require('../assets/images/card-image3.jpg'),
    cardImage4: require('../assets/images/card-image4.jpg'),
    cardImage5: require('../assets/images/card-image5.jpg')
}

export default images;