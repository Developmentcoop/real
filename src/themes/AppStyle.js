import { isIphoneX } from './Device';
import StyleSheet from '../components/RemStyleSheet';

const STATUSBAR_HEIGHT = isIphoneX ? 44 : 20;

export default StyleSheet.create({
    statusBar: {
        height: STATUSBAR_HEIGHT,
        borderBottomColor: '#4e4e4e',
        borderBottomWidth: 0.7
    }
});