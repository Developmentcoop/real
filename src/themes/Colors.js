const colors = {
    bottomBarColor: '#121212',
    statusBarColor: '#121212',
    headerColor: '#121212',
    cardHeadColor: '#121212',
    listBackgroundColor: '#000000',
    cardBackgroundColor: '#121212',
    swipeCardBackgroundColor: '#d8e3f0'
};

export default colors;