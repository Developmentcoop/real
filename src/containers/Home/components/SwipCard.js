import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';

import styles from '../HomeStyle';
import Images from '../../../themes/Images';

class SwipeCard extends React.Component {

    swipeLeft = () => {

    }

    swipeRight = () => {

    }

    render() {
        const { direction } = this.props;
        let source = direction === 'left' ? Images.swipeLeft : Images.swipeRight;

        return(
            <View style={styles.swipeCardContainer}>
                <View style={styles.swipeImageSection}>
                    <Image
                        source={source}
                        style={styles.swipeImage}
                    />
                </View>

                <Text style={styles.swipeDescSection}>Swipe Right to Like</Text>
            </View>
        );
    }
}

export default SwipeCard;