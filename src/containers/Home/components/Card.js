import React from 'react';
import {
    View,
    Text,
    Image,
    Alert
} from 'react-native';
import Toast from 'react-native-easy-toast';
import GestureRecognizer, { swipeDirection } from 'react-native-swipe-gestures';

import Images from '../../../themes/Images';
import styles from '../HomeStyle';
import SwipeCard from './SwipCard';

class Card extends React.Component {
    constructor(props) {
        super(props);
    }

    onSwipeLeft = () => {
        console.log('left');
        Alert.alert(
            'Confirm',
            'Like',
            [
                { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                { text: 'Ok', onPress: () => {}, style: 'ok' }
            ]
        );
    }

    onSwipeRight = () => {
        console.log('right');
        Alert.alert(
            'Confirm',
            'Dislike',
            [
                { text: 'Cancel', onPress: () => {}, style: 'cancel' },
                { text: 'Ok', onPress: () => {}, style: 'ok' }
            ]
        );
    }

    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 40
        };
        const { card } = this.props;

        return (
            <View>
                <View 
                    style={styles.cardHeader}>
                    <Image source={Images.posterImage} style={styles.cardHeaderProfileImage} />
                    <Text style={styles.cardHeaderName}>{card.label}</Text>
                </View>

                <View
                    style={{
                        position: 'relative'
                    }}
                >
                    <Image 
                        source={card.image}
                        style={{ width: '100%'}}
                        resizeMode='cover' 
                    />

                    <GestureRecognizer
                        onSwipeLeft={this.onSwipeLeft}
                        onSwipeRight={this.onSwipeRight}
                        config={config}
                        style={styles.swipeView}
                    >
                        <SwipeCard direction='left' />

                        <SwipeCard direction='right' />
                    </GestureRecognizer>
                </View>

                <View style={styles.cardFooter}>
                    <Image
                        source={Images.likeIcon}
                        style={styles.cardFooterImage}
                    />
                    <Image
                        source={Images.commentIcon}
                        style={styles.cardFooterImage}
                    />
                    <Image
                        source={Images.shareIcon}
                        style={styles.cardFooterImage}
                    />
                    <View style={{flex: 1}} />
                    <Text style={styles.cardPostTimeText}>
                        12 mins ago
                    </Text>
                </View>

                <View style={styles.cardCommentView}>
                    <Text
                        style={{
                            color: '#d8e3f0'
                        }}
                    >
                        6 likes
                    </Text>
                    <Text
                        style={{
                            color: '#575757',
                            marginTop: 5
                        }}
                    >
                        View 1 comment
                    </Text>
                </View>
            </View>
        );
    }
}

export default Card;