import StyleSheet from '../../components/RemStyleSheet';
// import { StyleSheet } from 'react-native';
import Color from '../../themes/Colors';

export default StyleSheet.create({
    header: {
        backgroundColor: Color.headerColor,
        flexDirection: 'row',
        justifyContent: 'space-between', 
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 4,
    },
    imageIconStyle: {
        width: 30, 
        height: 30,
    },
    titleStyle: {
        color: '#FFFFFF', 
        fontSize: 40
    },
    listStyle: {
        backgroundColor: Color.listBackgroundColor,
        paddingTop: 10, 
        paddingBottom: 6
    },
    itemStyle: {
        flexDirection: 'column',
        marginHorizontal: 4,
        alignItems: 'center',
    },

    cardContainer: {

    },
    cardHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Color.cardHeadColor,
        paddingHorizontal: 10,
        paddingVertical: 14
    },
    cardHeaderProfileImage: {
        width: 60, 
        height: 60, 
        borderRadius: 5
    },
    cardHeaderName: {
        color: '#FFFFFF',
        marginLeft: 15,
        fontSize: 16
    },
    cardFooter: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15, 
        paddingVertical: 5,
        backgroundColor: Color.cardBackgroundColor
    },
    cardFooterImage: {
        width: 30, 
        height: 30,
        marginRight: 15
    },
    cardPostTimeText: {
        color: '#575757',
    },
    cardCommentView: {
        paddingTop: 20,
        paddingBottom: 15, 
        paddingHorizontal: 15,
        backgroundColor: Color.cardBackgroundColor
    },

    swipeView: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 10
    },
    swipeCardContainer: {
        width: 120, 
        height: 120,
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: Color.swipeCardBackgroundColor,
        borderRadius: 5
    },
    swipeImageSection: {
        // width: 120,
        // height: 60,
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    swipeDescSection: {
        paddingVertical: 5,
        paddingHorizontal: 20,
        textAlign: 'center',
        backgroundColor: '#c3cdd8',
        borderRadius: 5
    },
    swipeImage: {
        width: 40, 
        height: 40
    }
});