import React from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    SafeAreaView,
    ScrollView,
    TouchableOpacity
} from 'react-native';

import Card from './components/Card';
import Images from '../../themes/Images';
import styles from './HomeStyle';
  
class Home extends React.Component {
    constructor() {
        super();

        this.state = {
            selectedCard: {
                label: 'Jessica',
                image: Images.cardImage1
            }
        };
    }

    renderProfiles = (data) => {
        const item = data.item;

        return (
            <TouchableOpacity 
                style={styles.itemStyle}
                onPress={() => this.setCurrentCard(item)} 
            >
                <Image 
                    source={Images.mediumProfile} 
                    style={{
                        width: 120, 
                        height: 120
                    }} 
                />
                <Text style={{color: 'white'}}>{item.label}</Text>
            </TouchableOpacity>
        );
    }

    setCurrentCard = (card) => {
        this.setState({
            selectedCard: card
        });
    }

    render() {
        const data = [
            {
                label: 'Jessica',
                image: Images.cardImage1
            },
            {
                label: 'Melissa',
                image: Images.cardImage2
            },
            {
                label: 'Monica',
                image: Images.cardImage3
            },
            {
                label: 'Jhoana',
                image: Images.cardImage4
            },
            {
                label: 'Narisa',
                image: Images.cardImage5
            }
        ];
        const { selectedCard } = this.state;

        return (
            <SafeAreaView>
                <ScrollView>
                    <View style={styles.header}>
                        <Image source={Images.plusIcon} style={styles.imageIconStyle} />
                        <Text style={styles.titleStyle}>REAL</Text>
                        <Image source={Images.messageIcon} style={styles.imageIconStyle} />
                    </View>

                    <View
                        style={{
                            alignItems: 'center'
                        }}
                    >
                        <FlatList 
                            data={data} 
                            horizontal 
                            renderItem={this.renderProfiles} 
                            keyExtractor={(item) => item.label}
                            style={styles.listStyle}
                            contentContainerStyle={{justifyContent: 'center', flexGrow: 1}}
                        />
                    </View>
                    
                    <Card card={selectedCard} />
                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default Home;