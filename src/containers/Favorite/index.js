import React from 'react';
import {
    View,
    Text
} from 'react-native';

import styles from './FavoriteStyle';

class Favorite extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <Text>Favorite Screen</Text>
            </View>
        );
    }
}

export default Favorite;