import React from 'react';

import StatusBar from '../../components/CustomStatusBar';
import AppNavigation from '../../navigations/AppNavigation';
import Color from '../../themes/Colors';

const App = () => {
  return (
    <>
      <StatusBar backgroundColor={Color.statusBarColor} barStyle='light-content' />
      <AppNavigation />
    </>
  );
};

export default App;