import React from 'react';
import {
    View,
    Text
} from 'react-native';

import styles from './SearchStyle';

class Search extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <Text>Search Screen</Text>
            </View>
        );
    }
}

export default Search;