import React from 'react';
import {
    View,
    Text
} from 'react-native';

import styles from './ProfileStyle';

class Profile extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <Text>Profile Screen</Text>
            </View>
        );
    }
}

export default Profile;