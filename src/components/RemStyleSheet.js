import { Dimensions, StyleSheet, Platform } from 'react-native';

const currentDeviceWidth = Dimensions.get('window').width;
const currentDeviceHeight = Dimensions.get('window').height;
const nexus5xWidth = 411;
const nexus5xHeight = 683;
const WIDTH_SCALE = currentDeviceWidth / nexus5xWidth;
const HEIGHT_SCALE = currentDeviceHeight / nexus5xHeight;

// properties will be zoom

const scaleProps = [
  'width',
  'height',
  'maxHeight',
  'minHeight',
  'margin',
  'marginLeft',
  'marginRight',
  'marginTop',
  'marginBottom',
  'padding',
  'paddingLeft',
  'paddingRight',
  'paddingTop',
  'paddingBottom',
  'top',
  'left',
  'right',
  'bottom',
  'fontSize',
  'borderRadius',
];

const widthProps = [
  'marginLeft',
  'marginHorizontal',
  'left',
  'height',
  'width',
  'borderRadius',
  'borderBottomWidth',
  'paddingHorizontal',
];

const heightProps = ['marginTop', 'paddingTop', 'paddingVertical', 'top'];
const StyleTransformer = {};

const styleTransformer = styleObj => {
  Object.keys(styleObj).forEach(key => {
    if (heightProps.includes(key)) {
      styleObj[key] = styleObj[key] * HEIGHT_SCALE;
    }
    if (widthProps.includes(key)) {
      styleObj[key] = styleObj[key] * WIDTH_SCALE;
    }
    if (key === 'fontSize') {
      if (Platform.OS === 'android')
        styleObj[key] = styleObj[key] * WIDTH_SCALE * 0.9;
      if (Platform.OS === 'ios') styleObj[key] = styleObj[key] * WIDTH_SCALE;
    }
  });
  return styleObj;
};

StyleTransformer.create = (styles = {}) => {
  Object.keys(styles).forEach(key => {
    styles[key] = styleTransformer(styles[key]);
  });
  return StyleSheet.create(styles);
};

export default StyleTransformer;

export const ts = v => v * scale;
