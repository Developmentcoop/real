import React from 'react';
import { 
    Text ,
    View,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

import Color from '../themes/Colors';

const TabBar = props => {
    const { 
        renderIcon,
        getLabelText,
        activeTintColor,
        inactiveTintColor,
        onTabPress,
        onTabLongPress,
        getAccessibilityLabel,
        navigation
    } = props;

    const { routes, index: activeRouteIndex } = navigation.state;

    return (
        <View style={style.container}>
            {
                routes.map((route, routeIndex) => {
                    const isRouteActive = routeIndex === activeRouteIndex;
                    const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;

                    return (
                        <View
                            key={routeIndex}
                            style={{flex: 1}}>
                            {isRouteActive ? <View style={{width: '50%', height: 3, backgroundColor: 'white', alignSelf: 'center'}} /> : null}
                            
                            <TouchableOpacity
                                key={routeIndex}
                                style={style.tabButton}
                                onPress={() => { onTabPress({ route }); }}
                                onLongPress={() => { onTabLongPress({ route }); }}
                                accessibilityLabel={getAccessibilityLabel({ route })}
                            >   
                                {renderIcon({ route, focused: isRouteActive, tintColor })}
                            </TouchableOpacity>
                        </View>
                    )
                })
            }
        </View>
    )
};

const style = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: Color.bottomBarColor,
        height: 52,
        elevation: 2,
        borderTopColor: '#4e4e4e',
        borderTopWidth: 0.3
    },
    tabButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default TabBar;